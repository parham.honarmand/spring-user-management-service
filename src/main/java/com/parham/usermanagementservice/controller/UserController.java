package com.parham.usermanagementservice.controller;

import com.parham.usermanagementservice.model.User;
import com.parham.usermanagementservice.service.Services;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {
    final Services services;

    public UserController(Services services) {
        this.services = services;
    }

    @PostMapping("/insert")
    public User insert(@Valid @RequestBody User user) {
        return services.insert(user);
    }

    @GetMapping("/get/{id}")
    public User getUserById(@PathVariable("id") Long id) {
        return services.getById(id);
    }

    @GetMapping("/getAll")
    public String getAll() {
        return services.findAll().toString();
    }

    @PutMapping("/update")
    public User update(@Valid @RequestBody User user){
        return services.update(user);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable("id") Long id){
        services.deleteById(id);
    }
}
