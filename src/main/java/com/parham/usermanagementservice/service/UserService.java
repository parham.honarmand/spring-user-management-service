package com.parham.usermanagementservice.service;

import com.parham.usermanagementservice.model.User;
import com.parham.usermanagementservice.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserService implements Services {
    final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User insert(User user) {
        return repository.save(user);
    }

    @Override
    public User getById(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public Collection<User> findAll() {
        return repository.findAll();
    }

    @Override
    public User update(User user) {
        return repository.save(user);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
