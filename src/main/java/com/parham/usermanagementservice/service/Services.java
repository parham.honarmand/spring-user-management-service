package com.parham.usermanagementservice.service;

import com.parham.usermanagementservice.model.User;

import java.util.Collection;

public interface Services {
    User insert(User user);

    User getById(Long id);

    Collection<User> findAll();

    User update(User user);

    void deleteById(Long id);
}
